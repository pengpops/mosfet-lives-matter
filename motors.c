//  Uses timer 2 interrupt to generate a square wave in pin
//  P2.0 and a 75% duty cycle wave in pin P2.1
//  Copyright (c) 2010-2018 Jesus Calvino-Fraga
//  ~C51~

//  Modified 2018 for Magnetic Field Controlled Robot project by Daniel Eng

#include <stdio.h>
#include <stdlib.h>
#include <EFM8LB1.h>

// ~C51~  

#define SYSCLK 72000000L
#define BAUDRATE 115200L

#define M1OUT0 P2_0
#define M1OUT1 P1_7

#define M2OUT0 P2_2
#define M2OUT1 P2_1

volatile unsigned char pwm_count=0;
volatile unsigned char pwm_count2=0;
volatile unsigned char pwm_set_motor1;
volatile unsigned char pwm_set_motor2;
volatile unsigned char reverse1;
volatile unsigned char reverse2;
volatile unsigned int valueMotor1;
volatile unsigned int valueMotor2;


//Function Prototypes
void setMotors(void);
int checkReverseMotor1 (int valueMotor1);
int checkReverseMotor2 (int valueMotor2);
void goRight (void);
void goLeft (void);
void goStraight (void);
void goBackwards (void);
//

char _c51_external_startup (void)
{
	// Disable Watchdog with key sequence
	SFRPAGE = 0x00;
	WDTCN = 0xDE; //First key
	WDTCN = 0xAD; //Second key
  
	VDM0CN=0x80;       // enable VDD monitor
	RSTSRC=0x02|0x04;  // Enable reset on missing clock detector and VDD

	#if (SYSCLK == 48000000L)	
		SFRPAGE = 0x10;
		PFE0CN  = 0x10; // SYSCLK < 50 MHz.
		SFRPAGE = 0x00;
	#elif (SYSCLK == 72000000L)
		SFRPAGE = 0x10;
		PFE0CN  = 0x20; // SYSCLK < 75 MHz.
		SFRPAGE = 0x00;
	#endif
	
	#if (SYSCLK == 12250000L)
		CLKSEL = 0x10;
		CLKSEL = 0x10;
		while ((CLKSEL & 0x80) == 0);
	#elif (SYSCLK == 24500000L)
		CLKSEL = 0x00;
		CLKSEL = 0x00;
		while ((CLKSEL & 0x80) == 0);
	#elif (SYSCLK == 48000000L)	
		// Before setting clock to 48 MHz, must transition to 24.5 MHz first
		CLKSEL = 0x00;
		CLKSEL = 0x00;
		while ((CLKSEL & 0x80) == 0);
		CLKSEL = 0x07;
		CLKSEL = 0x07;
		while ((CLKSEL & 0x80) == 0);
	#elif (SYSCLK == 72000000L)
		// Before setting clock to 72 MHz, must transition to 24.5 MHz first
		CLKSEL = 0x00;
		CLKSEL = 0x00;
		while ((CLKSEL & 0x80) == 0);
		CLKSEL = 0x03;
		CLKSEL = 0x03;
		while ((CLKSEL & 0x80) == 0);
	#else
		#error SYSCLK must be either 12250000L, 24500000L, 48000000L, or 72000000L
	#endif
	
	P0MDOUT |= 0x10; // Enable UART0 TX as push-pull output
	XBR0     = 0x01; // Enable UART0 on P0.4(TX) and P0.5(RX)                     
	XBR1     = 0X00;
	XBR2     = 0x40; // Enable crossbar and weak pull-ups

	// Configure Uart 0
	#if (((SYSCLK/BAUDRATE)/(2L*12L))>0xFFL)
		#error Timer 0 reload value is incorrect because (SYSCLK/BAUDRATE)/(2L*12L) > 0xFF
	#endif
	SCON0 = 0x10;
	TH1 = 0x100-((SYSCLK/BAUDRATE)/(2L*12L));
	TL1 = TH1;      // Init Timer1
	TMOD &= ~0xf0;  // TMOD: timer 1 in 8-bit auto-reload
	TMOD |=  0x20;                       
	TR1 = 1; // START Timer1
	TI = 1;  // Indicate TX0 ready

	// Initialize timer 2 for periodic interrupts
	TMR2CN0=0x00;   // Stop Timer2; Clear TF2;
	CKCON0|=0b_0001_0000; // Timer 2 uses the system clock
	TMR2RL=(0x10000L-(SYSCLK/10000L)); // Initialize reload value
	TMR2=0xffff;   // Set to reload immediately
	ET2=1;         // Enable Timer2 interrupts
	TR2=1;         // Start Timer2 (TMR2CN is bit addressable)

	EA=1; // Enable interrupts

  	
	return 0;
}

void Timer2_ISR (void) interrupt 5
{
	TF2H = 0; // Clear Timer2 interrupt flag
	
	pwm_count++;
	if(pwm_count>100) pwm_count=0;
	if(pwm_count2>100) pwm_count2=0;
	
	//MOUT0=pwm_count>50?0:1;
	if (!reverse1) {
	M1OUT0=0;
	M1OUT1=pwm_count>pwm_set_motor1?0:1;
	}
	else {
	M1OUT0=pwm_count>pwm_set_motor1?0:1;
	M1OUT1=0;
	}
	
	pwm_count2++;
	if (!reverse2) {
	M2OUT0=0;
	M2OUT1=pwm_count2>pwm_set_motor2?0:1;
	}
	else {
	M2OUT0=pwm_count2>pwm_set_motor2?0:1;
	M2OUT1=0;
	}
	
	
	
}


void setMotors(void) {			
	
	char direction;
	
	scanf("%c", &direction);
	
	if(direction == 'd') {
		goRight();
	}
	else if (direction == 'a') {
		goLeft();
	}
	else if (direction == 'w') {
		goStraight();
	}
	else if (direction == 's') {
		goBackwards();
	}
	else {
	valueMotor1 =0;
	valueMotor2 =0;
	}
	
	
	
	/*printf("Set PWM Value for motor 1 and 2\n");
	scanf("%d %d", &valueMotor1, &valueMotor2);
	printf("Value was %d and %d pwm", valueMotor1, valueMotor2);*/
	
	reverse1 = checkReverseMotor1(valueMotor1);
	if (!reverse1) {
		pwm_set_motor1 = valueMotor1;
	}
	else {
		pwm_set_motor1 = -1 * valueMotor1;
	}
	
	reverse2 = checkReverseMotor2(valueMotor2);
	if (!reverse2) {
		pwm_set_motor2 = valueMotor2;
	}
	else {
		pwm_set_motor2 = -1 * valueMotor2;
	}
	
	
}

int checkReverseMotor1 (int valueMotor1) {		//Reverse flag for motor 1
	int reverseFlag;
	if (valueMotor1>=0) {
	reverseFlag = 0;
	}
	else {
	reverseFlag = 1;
	}
	
	return reverseFlag;
}

int checkReverseMotor2 (int valueMotor2) {		//Reverse flag for motor 2
	int reverseFlag;
	if (valueMotor2>=0) {
	reverseFlag = 0;
	}
	else {
	reverseFlag = 1;
	}
	
	return reverseFlag;
}

//Functions to set motor PWM values
void goRight (void) {
	valueMotor1 = 50;
	valueMotor2 = 100;
}

void goLeft (void) {
	valueMotor1 = 100;
	valueMotor2 = 50;
}

void goStraight (void) {
	valueMotor1= 100;
	valueMotor2 = 100;
}

void goBackwards (void) {
	valueMotor1 = -100;
	valueMotor2= -100;
}



void main (void)
{
	
//	int pwmval;
	//Initialize motor speeds to 0
	pwm_set_motor1 = 0;
	pwm_set_motor2 = 0;
	
	
	printf("\x1b[2J"); // Clear screen using ANSI escape sequence.
	printf("Square wave generator for the EFM8LB1.\r\n"
	       "Check pins P2.0 and P2.1 with the oscilloscope.\r\n");
	       
	       
	       
	while(1)
	{
		printf("\x1b[2J"); 					//Clear Screen
		printf("Press 'a' to go left\n");
		printf("Press 'd' to go right\n");
		printf("Press 'w' to go straight\n");
		printf("Press 's' to go down\n");
		setMotors();
	}
}