//  Receives commands via a 16-bit 1-wire signal

#include <EFM8LB1.h>	// !!not valid for the STM32 chip
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define SYSCLK    72000000L // SYSCLK frequency in Hz
#define BAUDRATE  115200L   // Baud rate of UART in bps
#define DEFAULT_F 4000L
#define MAGBT	  6		// 6 ms baud period
#define TADJ	  925	// period calibration

#define SGNP P1_5

#define M1OUT0 P2_0
#define M1OUT1 P1_7

#define M2OUT0 P2_2
#define M2OUT1 P2_1


volatile char sequence[12] = { 0, 0,				//Placeholder array; values do not matter
								0, 0, 0, 0, 0,
								0, 0, 0, 0, 0 };
char motor1Array[] = {0, 0, 0, 0, 0};
char motor2Array[] = {0, 0, 0, 0, 0};

volatile bit sigIn;

volatile char command;
volatile char lockCar = 0;

volatile unsigned char pwm_count=0;
volatile unsigned char pwm_count2=0;
volatile unsigned char pwm_set_motor1=0;
volatile unsigned char pwm_set_motor2=0;
volatile unsigned char reverse1;
volatile unsigned char reverse2;
volatile unsigned int valueMotor1;
volatile unsigned int valueMotor2;



char overflow_count;

int binaryinteger(char *string);

char _c51_external_startup(void)
{
	// Disable Watchdog with key sequence
	SFRPAGE = 0x00;
	WDTCN = 0xDE; //First key
	WDTCN = 0xAD; //Second key

	VDM0CN |= 0x80;
	RSTSRC = 0x02;

#if (SYSCLK == 48000000L)	
	SFRPAGE = 0x10;
	PFE0CN = 0x10; // SYSCLK < 50 MHz.
	SFRPAGE = 0x00;
#elif (SYSCLK == 72000000L)
	SFRPAGE = 0x10;
	PFE0CN = 0x20; // SYSCLK < 75 MHz.
	SFRPAGE = 0x00;
#endif

#if (SYSCLK == 12250000L)
	CLKSEL = 0x10;
	CLKSEL = 0x10;
	while ((CLKSEL & 0x80) == 0);
#elif (SYSCLK == 24500000L)
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0);
#elif (SYSCLK == 48000000L)	
	// Before setting clock to 48 MHz, must transition to 24.5 MHz first
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0);
	CLKSEL = 0x07;
	CLKSEL = 0x07;
	while ((CLKSEL & 0x80) == 0);
#elif (SYSCLK == 72000000L)
	// Before setting clock to 72 MHz, must transition to 24.5 MHz first
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0);
	CLKSEL = 0x03;
	CLKSEL = 0x03;
	while ((CLKSEL & 0x80) == 0);
#else
#error SYSCLK must be either 12250000L, 24500000L, 48000000L, or 72000000L
#endif

	// Configure the pins used for square output
//	P2MDOUT |= 0b_0000_0011;
	P0MDOUT |= 0x10; // Enable UART0 TX as push-pull output
	XBR0 = 0x01; // Enable UART0 on P0.4(TX) and P0.5(RX)                     
	XBR1 = 0X10; // Enable T0 on P0.0
	XBR2 = 0x40; // Enable crossbar and weak pull-ups

#if (((SYSCLK/BAUDRATE)/(2L*12L))>0xFFL)
#error Timer 0 reload value is incorrect because (SYSCLK/BAUDRATE)/(2L*12L) > 0xFF
#endif
				 // Configure Uart 0
	SCON0 = 0x10;
	CKCON0 |= 0b_0000_0000; // Timer 1 uses the system clock divided by 12.
	TH1 = 0x100 - ((SYSCLK / BAUDRATE) / (2L * 12L));
	TL1 = TH1;      // Init Timer1
	TMOD &= ~0xf0;  // TMOD: timer 1 in 8-bit auto-reload
	TMOD |= 0x20;
	TR1 = 1; // START Timer1
	TI = 1;  // Indicate TX0 ready

			 // Initialize timer 2 for periodic interrupts
	TMR2CN0 = 0x00;   // Stop Timer2; Clear TF2;
	CKCON0 |= 0b_0001_0000;
//	TMR2RL = (-(SYSCLK / (2 * DEFAULT_F))); // Initialize reload value
	TMR2RL=(0x10000L-(SYSCLK/10000L)); // Initialize reload value
	TMR2 = 0xffff;   // Set to reload immediately
	ET2 = 1;         // Enable Timer2 interrupts
	TR2 = 1;         // Start Timer2
	EA = 1; // Global interrupt enable

	return 0;
}
void TIMER0_Init(void)
{
	TMOD &= 0b_1111_0000; // Set the bits of Timer/Counter 0 to zero
	TMOD |= 0b_0000_0001; // Timer/Counter 0 used as a 16-bit timer that runs on sysclk
	TR0 = 0; // Stop Timer/Counter 0
}
void Timer3us(unsigned char us)
{
	unsigned char i;               // usec counter

								   // The input for Timer 3 is selected as SYSCLK by setting T3ML (bit 6) of CKCON0:
	CKCON0 |= 0b_0100_0000;

	TMR3RL = (-(SYSCLK) / 1000000L); // Set Timer3 to overflow in 1us.
	TMR3 = TMR3RL;                 // Initialize Timer3 for first overflow

	TMR3CN0 = 0x04;                 // Sart Timer3 and clear overflow flag
	for (i = 0; i < us; i++)       // Count <us> overflows
	{
		while (!(TMR3CN0 & 0x80));  // Wait for overflow
		TMR3CN0 &= ~(0x80);         // Clear overflow indicator
	}
	TMR3CN0 = 0;                   // Stop Timer3 and clear overflow flag
}


void Timer2_ISR (void) interrupt 5
{
	TF2H = 0; // Clear Timer2 interrupt flag
	sigIn = SGNP;
	

	pwm_count++;
	if(pwm_count>100) pwm_count=0;
	if(pwm_count2>100) pwm_count2=0;
	
	if (!reverse1) {
	M1OUT0=0;
	M1OUT1=pwm_count>pwm_set_motor1?0:1;
	}
	else {
	M1OUT0=pwm_count>pwm_set_motor1?0:1;
	M1OUT1=0;
	}
	
	pwm_count2++;
	if (!reverse2) {
	M2OUT0=0;
	M2OUT1=pwm_count2>pwm_set_motor2?0:1;
	}
	else {
	M2OUT0=pwm_count2>pwm_set_motor2?0:1;
	M2OUT1=0;
	}
	
	
}


void waitms(unsigned int ms)
{
	unsigned int j;
	unsigned char k;
	for (j = 0; j<ms; j++)
		for (k = 0; k<4; k++) Timer3us(250);
}
void waitus(unsigned int us) {
	char loops = us/250;
	char remainder = us%250;
	while (loops--){
		Timer3us(250);
	}
	Timer3us(remainder);
}

/*void Timer2_ISR(void) interrupt INTERRUPT_TIMER2
{
	TF2H = 0; // Clear Timer2 interrupt flag
	// this ISR does the command reception
	sigIn = SGNP;
}*/

void resetT0(void) {
	TF0 = 0;
	TL0 = 0;
	TH0 = 0;
	overflow_count = 0;
}


float avgBaud() {

	float avg = 0;
	
	char i = 0;
	//P1_6 = !P1_6;
	// wait 2 periods,
	waitus(MAGBT * TADJ / 9 );
	waitus(MAGBT * TADJ / 9 );
	// read the bit 5 times
	for (i = 0; i < 5; i++) {
		waitus(MAGBT * TADJ / 9 );
		avg += sigIn;
	}
	// wait another 2 periods for a total of 9
	waitus(MAGBT * TADJ / 9 );
	waitus(MAGBT * TADJ / 9 );
	return avg/5.0;
}

char getLeftSpeed(void) {
	return 0;
}

int binaryinteger(char string[]) {		//Binary to int function

	int integer = 0;
	int j = 3;
	int i = 0;
	for (i = 0; i<4; i++) {
		if (string[i] == 1) {
			integer = (int) integer + powf(2.0, j);
		}
		j--;
	}
	
	return integer*100/15.0;
	
	//Max 100 
	//Min 0
}

void extract4bit(void) {
	
	if (sequence[0] == 1 && sequence[1] == 1) {		//11 in first two bits to set motors	
		command = 1; //This is to set motors;
	}
	else if (sequence[0] == 1 && sequence[1] == 0 && sequence[2] == 0 && sequence[3] == 1)	{	//10 in first two bits to lock car
		lockCar = 1;
	}
	else if (sequence[0] == 0 && sequence[1] == 1 && sequence[2] == 1 && sequence[3] == 0) {
		lockCar = 0;
	}
	
	motor1Array[0] = sequence[2];		//Motor Reverse bit
	motor1Array[1] = sequence[3];
	motor1Array[2] = sequence[4];
	motor1Array[3] = sequence[5];
	motor1Array[4] = sequence[6];
	
	motor2Array[0] = sequence[7];		//Motor reverse bit
	motor2Array[1] = sequence[8];
	motor2Array[2] = sequence[9];
	motor2Array[3] = sequence[10];
	motor2Array[4] = sequence[11];
}
 
void doStuffWithMotor1 (void) {
	

}

void doStuffWithMotor2 (void) {

}


void main(void)
{
	float newBit = 0;
	char i = 0;
	bit validSignal = 0;

	//Initialize motor speeds to 0
	pwm_set_motor1 = 0;
	pwm_set_motor2 = 0;

	printf("\x1b[2J");			// Clear screen using ANSI escape sequence.
	
	/* test script for timer0*/
		/*
		resetT0();
		while (P1_4 == 1){
			TR0 = 1; // Start Timer/Counter 0
			if (TF0 == 1) // Did the 16-bit timer overflow?
			{
				TF0 = 0;
				overflow_count++;
			}
		}
		printf("%f \n", (overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK) );
		while (P1_4 == 0) {}
	*/
	
	while (1) {

		/*========================================================
		|	this is the signal decoder loop
		|	the signature is "1 0 1 0"
		|	the first two bits are used to reject noise
		|	the last two bits are used to align the clock
		|	it is triggered as soon as '1' is detected
		=========================================================*/

	decode:
		P1_6 = 0;
		waitms(1);
		while (SGNP == 0){}
		if (sigIn == 1) {	// Start with finding a '1'
			validSignal = 1;
			P1_6 = 1;

			resetT0();
			TR0 = 1; // Start Timer/Counter 0
			while (sigIn == 1) { //While loop waiting for a '0'
				if (TF0 == 1) // Did the 16-bit timer overflow?
				{
					TF0 = 0;
					overflow_count++;
				}
				if ((overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK) > 1.3*MAGBT / 1000) {	// if no low bit within 1.2 periods
					validSignal = 0;
					P1_6 = 0;
					printf("Failed while waiting for 0: %f \n", (overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK));
					resetT0();
					goto decode;
				}
			} 
		//	printf("Found 0: %f \n", (overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK));
			resetT0();
			TR0 = 1; // Start Timer/Counter 0, waiting until next '1' 
			while (sigIn == 0) { //While loop waiting for a '1'
				if (TF0 == 1) // Did the 16-bit timer overflow?
				{
					TF0 = 0;
					overflow_count++;
				}
				if ((overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK) > 1.3*MAGBT / 1000) {	// if no high bit within 1.5 periods
					validSignal = 0;
					P1_6 = 0;
					printf("Failed while waiting for 1: %f\n", (overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK));
					resetT0();
					goto decode;
				}
			}
		//	printf("Found 1: %f \n", (overflow_count*65536.0 + TH0*256.0 + TL0)*(1.5 / SYSCLK));
			resetT0();
			
			while (sigIn == 1) {}	// wait for signal to reach 0 then start reading
			
			newBit = avgBaud();
			if (newBit > 0.8) {
				// reject if the final signature bit isn't '0'
				printf("Final bit not 0: %f \n", newBit);
				validSignal = 0;
				P1_6 = 0;
				goto decode;
			}
		}

		if (validSignal == 1){
		
			// bits
			// waitus(72000);
			
			for (i = 0; i < 12; i++) {
				newBit = avgBaud();
				sequence[i] = newBit >= 0.8;
				//printf("Read bit with avg: %f \n", newBit);
			}
			
			validSignal = 0;
			P1_6 = 0;
			
			for (i=0; i < 12; i++) {
				printf("%d", sequence[i]);
			}

			printf("\n");
			
			extract4bit();
				
			if (lockCar == 0){
				if(command == 1) {

				pwm_set_motor1 = binaryinteger(motor1Array + 1);
				pwm_set_motor2 = binaryinteger(motor2Array + 1);
				
				reverse1 = motor1Array[0];
				reverse2 = motor2Array[0];
				//printf("\x1b[2J");
				printf("Left reverse: %d; Right reverse: %d\n", reverse1, reverse2);
				}
			}
			else {
			
				pwm_set_motor1 = 0;
				pwm_set_motor2 = 0;
				printf("Car is locked");

			}
		
		
		}

	}
}