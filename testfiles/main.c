#include "stm32f05xxx.h"
#include <stdio.h>
#include <stdlib.h>

#define SYSCLK 48000000L
#define DEF_F 21420L
#define MAGBT	6000 * 4.12
volatile char osc = 0;
volatile char counter = 0;
char motorRight[5] = { 0, 0, 0, 0, 0 };
char motorLeft[5] = { 0, 0, 0, 0, 0 };
char sequence[12] = { 1, 1,				//Test Sequence to go Straight
					1, 0, 1, 1, 1,
					1, 0, 1, 1, 1 };
void SysInit(void)
{
	// Set up output port bit for blinking LED
	RCC_AHBENR |= 0x00020000;  // peripheral clock enable for port A
	GPIOA_MODER |= (1); // Make pin PA0 output
	GPIOA_MODER |= (0 << 1); //
	GPIOA_MODER |= (1 << 2); // Make pin PA1 output
	GPIOA_MODER |= (0 << 3); // 
	GPIOA_MODER |= (1 << 4); // Make pin PA2 output
	GPIOA_MODER |= (0 << 5); // 
	


	// Set up timer
	RCC_APB2ENR |= BIT11; // turn on clock for timer1
	TIM1_ARR = SYSCLK/(DEF_F*2L);
	ISER |= BIT13;        // enable timer interrupts in the NVIC
	TIM1_CR1 |= BIT4;     // Downcounting    
	TIM1_CR1 |= BIT0;     // enable counting    
	TIM1_DIER |= BIT0;    // enable update event (reload event) interrupt  
	enable_interrupts(); 
}

void initADC(void)
{
	RCC_AHBENR |= BIT17;   		// Turn on GPIOA
	RCC_AHBENR |= BIT18;        // Turn on GPIOB
	RCC_APB2ENR |= BIT9;        // Turn on ADC 
	GPIOA_MODER |= (BIT14+BIT15);// Select analog mode for PA7 (pin 13 of LQFP32 package) **Test, read HORZ on right joystick
	GPIOB_MODER |= (BIT0+BIT1); //Pin 14 HORZ for left joystick PB0
	GPIOB_MODER |= (BIT2+BIT3); //Pin 15 VERT for left joystick PB1
	GPIOA_MODER |= (BIT8+BIT9);
	GPIOA_MODER |= (BIT12+BIT13);
	ADC_CR |= BIT31;            // Begin ADCCalibration
	while ((ADC_CR & BIT31));   // Wait for calibration complete
	ADC_SMPR=7;                 // Long sampling time for more stable measurements
	//ADC_CHSELR |= BIT17;      // Select Channel 17, internal reference
	ADC_CHSELR |= BIT9;         // Select Channel 9 pin 15? -> Control which channel to read through read_adc
	ADC_CCR |= BIT22;	        // Enable the reference voltage
	ADC_CR |= BIT0;             // Enable the ADC
}

int readADC(char channel)
{
	//channel 9 = pin 15 PB1 LEFTx
	//channel 8 = pin 14 PB0 LEFTy
	//channel 7 = pin 13 PA7 RIGHTx
	
	ADC_CHSELR &= ~(0xFFFF); 			// Clears channels 0-15
	ADC_CHSELR |= 1 << channel;         // Select Channel 
	ADC_CR |=  BIT2;            // Trigger a conversion
	while ( (ADC_CR & BIT2) );  // Wait for End of Conversion
	return ADC_DR;              // ADC_DR has the 12 bits out of the ADC

}

void delay(int dly)
{
	while( dly--);
}

void TogglePin(void) 
{    
	if (osc == 1)
		GPIOA_ODR ^= BIT1 | BIT2; // Toggle PA0
}


void transmit_serial(char serial[12]) {
	// transmit the header, 1010
	char i = 0;
	osc = 1;
	delay(MAGBT);
	osc = 0;
	delay(MAGBT);
	osc = 1;
	delay(MAGBT);
	osc = 0;
	delay(MAGBT);

	for (i = 0; i < 12; i++) {
		osc = serial[i];
		delay(MAGBT);
	}
	osc = 0;
}

void Timer1ISR(void) 
{
	TIM1_SR &= ~BIT0; // clear update interrupt flag
	TogglePin();
}

void intToBinary(int MotorLeft, int MotorRight)
{
	//Use arrayTest [3] to [6] for Motor 1
	//Use arrayTest [8] to [11] for Motor 2

	int Motor1 = MotorLeft;
	int Motor2 = MotorRight;


	sequence[6] = Motor1 % 2;
	Motor1 = Motor1 / 2;
	sequence[5] = Motor1 % 2;
	Motor1 = Motor1 / 2;
	sequence[4] = Motor1 % 2;
	Motor1 = Motor1 / 2;
	sequence[3] = Motor1 % 2;

	sequence[11] = Motor2 % 2;
	Motor2 = Motor2 / 2;
	sequence[10] = Motor2 % 2;
	Motor2 = Motor2 / 2;
	sequence[9] = Motor2 % 2;
	Motor2 = Motor2 / 2;
	sequence[8] = Motor2 % 2;

}

void main(void)
{
	char buf[32];
	char direction;

	int left_x=2;
	int left_y=2;
	int right_x=2;
	int lock=3000;
	int unlock = 3000;

	float x1;
	float y1;
	float x2;
	
	int motorLeft = 0;
	int motorRight = 0;
	
	char reverse1;
	char reverse2;
	
	GPIOA_ODR |= BIT1;
	GPIOA_ODR &= ~(BIT2);
	SysInit();
	initADC();

	
	while (1)
	{
		osc = 0;
		
		left_y = readADC(9); //Reading values from ADC from joystick
		left_x = readADC(8);
		right_x = readADC(7);
		lock = readADC(4);
		unlock = readADC(6);
		


		if(lock < 2048){
			sequence[0] = 1;
			sequence[1] = 0;
			sequence[2] = 0;
			sequence[3] = 1;
			for(int i = 0; i < 3; i++){
				GPIOA_ODR |= BIT0;
				transmit_serial(sequence);
				GPIOA_ODR &= ~BIT0;
				delay(400000);
			}
		}


		if(unlock < 2048) {
			sequence[0] = 0;
			sequence[1] = 1;
			sequence[2] = 1;
			sequence[3] = 0;
			for(int i = 0; i < 3; i++){
				GPIOA_ODR |= BIT0;
				transmit_serial(sequence);
				GPIOA_ODR &= ~BIT0;
				delay(400000);
			}
		} 


		x1 = left_x / 2048.0 - 1;
		y1 = 1 - left_y / 2048.0;
		x2 = right_x / 2048.0 - 1;
		
		if (y1 > 0){ //For moving forward
			sequence[2] = 0;
			sequence[7] = 0;
			if (x1 < 0){
				//turning left so slowing down left motor speed
				motorLeft = 15 * y1 * (1 + x1);
				motorRight = 15 * y1;	//Full speed
				}
			else {
				motorRight = 15 * y1 * (1 - x1);
				motorLeft = 15 * y1; 	//Full speed
				}	
			}
		
		else { //for moving backward
			sequence[2] = 1;
			sequence[7] = 1;
			if (x1 < 0){
				//turning left so slowing down left motor speed
				motorLeft = 0 - 15 * y1 * (1 + x1);
				motorRight = 0 - 15 * y1;	//Full speed
				}
			else {
				motorRight = 0 - 15 * y1 * (1 - x1);
				motorLeft = 0 - 15 * y1; 	//Full speed
				}	
		}
		
		if (!((x2 < 0.05) && (x2 > -0.05))){
			if (x2 < 0){ //Right Joystick
				sequence[2] = 1;
				sequence[7] = 0;
				motorLeft = -15 * x2;
				motorRight = -15 *x2;
			}
			else{ 
				sequence[2] = 0;
				sequence[7] = 1;
				motorLeft = 15 * x2;
				motorRight = 15 * x2;
			}
		}
		
		
		printf("\x1b[2J");
		printf("MotorLeft: %d\r\n", motorLeft);
		printf("MotorRight: %d\r\n", motorRight);
		
		intToBinary(motorLeft, motorRight);
		

		sequence[0] = 1;
		sequence[1] = 1;


		for (int i = 0; i<12; i++) {
			printf("%d", sequence[i]);
		}

		printf("\n");

		printf("%d and %d", sequence[2], sequence[7]);
		printf("\n");

		GPIOA_ODR |= BIT0;
		transmit_serial(sequence);
		GPIOA_ODR &= ~BIT0;
		delay(400000);
		fflush(stdout);	
	
	}
}
/*
char egetc(void);
int egets(char *s, int Max);



char buff[32];


int egets(char *s, int Max);


*/