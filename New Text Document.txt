//  Receives commands via a 16-bit 1-wire signal

#include <EFM8LB1.h>	// !!not valid for the STM32 chip
#include <stdlib.h>
#include <stdio.h>

#define SYSCLK    72000000L // SYSCLK frequency in Hz
#define BAUDRATE  115200L   // Baud rate of UART in bps
#define DEFAULT_F 5000L
#define MAGBT	  1000/100

#define SGNP P2_0

volatile char sequence[12] = { 1, 1,
								0, 0, 1, 1, 1,
								0, 0, 1, 1, 0 };
volatile bit sigIn;

char _c51_external_startup(void)
{
	// Disable Watchdog with key sequence
	SFRPAGE = 0x00;
	WDTCN = 0xDE; //First key
	WDTCN = 0xAD; //Second key

	VDM0CN |= 0x80;
	RSTSRC = 0x02;

#if (SYSCLK == 48000000L)	
	SFRPAGE = 0x10;
	PFE0CN = 0x10; // SYSCLK < 50 MHz.
	SFRPAGE = 0x00;
#elif (SYSCLK == 72000000L)
	SFRPAGE = 0x10;
	PFE0CN = 0x20; // SYSCLK < 75 MHz.
	SFRPAGE = 0x00;
#endif

#if (SYSCLK == 12250000L)
	CLKSEL = 0x10;
	CLKSEL = 0x10;
	while ((CLKSEL & 0x80) == 0);
#elif (SYSCLK == 24500000L)
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0);
#elif (SYSCLK == 48000000L)	
	// Before setting clock to 48 MHz, must transition to 24.5 MHz first
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0);
	CLKSEL = 0x07;
	CLKSEL = 0x07;
	while ((CLKSEL & 0x80) == 0);
#elif (SYSCLK == 72000000L)
	// Before setting clock to 72 MHz, must transition to 24.5 MHz first
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0);
	CLKSEL = 0x03;
	CLKSEL = 0x03;
	while ((CLKSEL & 0x80) == 0);
#else
#error SYSCLK must be either 12250000L, 24500000L, 48000000L, or 72000000L
#endif

	// Configure the pins used for square output
	P2MDOUT |= 0b_0000_0011;
	P0MDOUT |= 0x10; // Enable UART0 TX as push-pull output
	XBR0 = 0x01; // Enable UART0 on P0.4(TX) and P0.5(RX)                     
	XBR1 = 0X10; // Enable T0 on P0.0
	XBR2 = 0x40; // Enable crossbar and weak pull-ups

#if (((SYSCLK/BAUDRATE)/(2L*12L))>0xFFL)
#error Timer 0 reload value is incorrect because (SYSCLK/BAUDRATE)/(2L*12L) > 0xFF
#endif
				 // Configure Uart 0
	SCON0 = 0x10;
	CKCON0 |= 0b_0000_0000; // Timer 1 uses the system clock divided by 12.
	TH1 = 0x100 - ((SYSCLK / BAUDRATE) / (2L * 12L));
	TL1 = TH1;      // Init Timer1
	TMOD &= ~0xf0;  // TMOD: timer 1 in 8-bit auto-reload
	TMOD |= 0x20;
	TR1 = 1; // START Timer1
	TI = 1;  // Indicate TX0 ready

			 // Initialize timer 2 for periodic interrupts
	TMR2CN0 = 0x00;   // Stop Timer2; Clear TF2;
	CKCON0 |= 0b_0001_0000;
	TMR2RL = (-(SYSCLK / (2 * DEFAULT_F))); // Initialize reload value
	TMR2 = 0xffff;   // Set to reload immediately
	ET2 = 1;         // Enable Timer2 interrupts
	TR2 = 1;         // Start Timer2
	EA = 1; // Global interrupt enable

	return 0;
}
void TIMER0_Init(void)
{
	TMOD &= 0b_1111_0000; // Set the bits of Timer/Counter 0 to zero
	TMOD |= 0b_0000_0001; // Timer/Counter 0 used as a 16-bit timer that runs on sysclk
	TR0 = 0; // Stop Timer/Counter 0
}
void Timer3us(unsigned char us)
{
	unsigned char i;               // usec counter

								   // The input for Timer 3 is selected as SYSCLK by setting T3ML (bit 6) of CKCON0:
	CKCON0 |= 0b_0100_0000;

	TMR3RL = (-(SYSCLK) / 1000000L); // Set Timer3 to overflow in 1us.
	TMR3 = TMR3RL;                 // Initialize Timer3 for first overflow

	TMR3CN0 = 0x04;                 // Sart Timer3 and clear overflow flag
	for (i = 0; i < us; i++)       // Count <us> overflows
	{
		while (!(TMR3CN0 & 0x80));  // Wait for overflow
		TMR3CN0 &= ~(0x80);         // Clear overflow indicator
	}
	TMR3CN0 = 0;                   // Stop Timer3 and clear overflow flag
}
void waitms(unsigned int ms)
{
	unsigned int j;
	unsigned char k;
	for (j = 0; j<ms; j++)
		for (k = 0; k<4; k++) Timer3us(250);
}
void Timer2_ISR(void) interrupt INTERRUPT_TIMER2
{
	TF2H = 0; // Clear Timer2 interrupt flag
	// this ISR does the command reception
	sigIn = SGNP;
}

char getLeftSpeed(void) {

}

void main(void)
{
	bit validSignal = 0;
	char i = 0;

	while (1) {
		// this is the signal decoder loop
		if (sigIn == 1) {
			validSignal = 1;
			while (sigIn == 1) {}
			// start counting time until the next '1'
			overflow_count = 0;
			TR0 = 1; // Start Timer/Counter 0
			while (signIn == 0) {
				if (TF0 == 1) // Did the 16-bit timer overflow?
				{
					TF0 = 0;
					overflow_count++;
					if ((overflow_count*65536.0)*(1.5 / SYSCLK) > 2*MAGBT) {	// if no high bit within 2 periods
						validSignal = 0;
						break;
					}
				}
			}
		}

		if (validSignal = 1){
			while (sigIn == 1){}	// wait for signal to reach 0 then start reading
			waitms(10);
				// bits
			for (i = 0; i < 11; i++) {
				waitms(5);
				sequence[i] = sigIn;
			}
		}
	
	}
}